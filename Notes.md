This file is for taking notes as you go.

1. After loading the project into vscode and installing the recommened go packages, I noticed this error in the go.mod file
    ```
    err: exit status 1: stderr: go: updates to go.mod needed, disabled by -mod=readonly; to update it:
        go mod tidy
    : packages.Load errorgo list
    ```
    ran `go mod tidy` per vscodes recommendation
2. Backend Test Issue
    Ran the backend test `go test ./server -v`. Got the error message
    ```
    === RUN   TestServerSuite
    === RUN   TestServerSuite/TestHealthCheck
    2022/08/31 21:37:41 starting server on port 3000
        main_test.go:55: 
                    Error Trace:    main_test.go:55
                    Error:          "{\"OK\":true}\n" does not contain "\"ok\":true"
                    Test:           TestServerSuite/TestHealthCheck
    === RUN   TestServerSuite/TestWebsockets
    --- FAIL: TestServerSuite (0.01s)
        --- FAIL: TestServerSuite/TestHealthCheck (0.01s)
        --- PASS: TestServerSuite/TestWebsockets (0.00s)
    FAIL
    FAIL    gitlab.com/upchieve/two-am-takeout/server       0.381s
    FAIL
    ```
    At first thought there might be something wrong with the test itself (casing in expected value) but then noticed the hint in the README `(hint: it's related to struct tags)`.
    So I then traced the response to the `handlers.go` file. Figured that `HealthBody` struct there must be the cause of the issue. I noticed that it's using `` OK bool `json:"OK"` `` and got to the `Using Struct Tags to Control Encoding` section of the linked doc. Realized I just needed to change the casing of the struct tag, the `json:"OK"` to `json:"ok"`. And :tada: the backend test is working.
3. Backend Test Assumption
    I did a quick skim through of the `main.go` and `main_test.go` files. It didn't seem like anything was wrong with the TestWebsockets tests, at least that I could tell. So I started to focus more on the TestHealthCheck test. I started up the backend and frontend and immediately noticed in the browser tools console the `/health` request was consistently failing due to cors
    ```
    Access to XMLHttpRequest at 'http://localhost:3000/health' from origin 'http://localhost:8081' has been blocked by CORS policy: The 'Access-Control-Allow-Origin' header has a value 'http://localhost:3000' that is not equal to the supplied origin.
    ```
    I updated the `TestHealthCheck` test to add a request Origin header with a value of `http://localhost:8080` and added an assertion check to ensure that the request `Origin` and Response `Access-Control-Allow-Origin` match.
    After making sure the test failed I then updated the `Access-Control-Allow-Origin` to `http://localhost:8080` in the `ServeHealth` function in the `handlers.go`.
4. Frontend
    To ensure users can enter a message that stays I started the backend and frontend again. Entered some test text and noticed in the browser tools console that there is an issue with the websocket
    ```
    WebSocket is already in CLOSING or CLOSED state
    ```
    Took a look at the `store/index.js` file as well as the `main.js` file and looked up the confiuration options for `https://github.com/nathantsoi/vue-native-websocket` and then started to really wonder about the format of the message and noticed that the `sendMessage` function sends a string to the socket rather than the expected json. so in the main.js file I removed the format key and value and then we were golden!
5. Security Issue Thoughts
    As far as security concerns my first thought was DDOS attacks which could be prevented by having a seperate process that keeps track of remote IPs being registered in the hub and compares the remote IP address of a new request with existing connected IPs. But since that would be a seperate process I then looked the code over a bit and noticed the `CORSMethodMiddleware` method in the `main.go` file. Read through the methods description and noticed it's essentially useless until a handler allows OPTIONS requests.
6. Frontend Style
    Had a little fun remembering how to do things with Vue.