import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import VueNativeSock from 'vue-native-websocket'

Vue.config.productionTip = false
Vue.use(
  VueNativeSock,
  'ws://localhost:3000/ws',
  { store }
)

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
